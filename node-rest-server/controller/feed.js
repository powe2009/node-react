const mongoose = require('mongoose');
const Post = require('../models/post');
const {validationResult} = require('express-validator');

exports.getPosts = (req, res, next) => {
    Post.find()
        .then(posts => {
            res.status(200)
                .json({posts: posts})
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err)
        });       
};

exports.updatePost = (req, res, next) => {
    // put content
    const postId = req.paras.postId;
    const title = req.body.title;
    const content = req.body.content;
    let imgUrl = req.body.imageUrl;
    if (req.file) {
        imageUrl = req.body.image;
    } 
    if (!image.utl) {
        const error = new Error('No file picked');
        error.statusCode = 422;
        throw error;
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed');   
        error.statusCode = 422;
        throw error;    

    }
}

exports.createPost = (req, res, next) => {
    // post content
    const title = req.body.title;
    const content = req.body.content;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        const error = new Error('Validation failed');   
        error.statusCode = 422;
        throw error;    

        // return res.status(422)
        //  .json({
        //     message: 'validation errors', 
        //     errors:arrors.array()
        //   });
    }

    if (!req.file) {
        const error = new Error('No image file');
        error.statusCode = 422;
        throw error;
    }

    const imageUrl = req.file.path;

    const post = new Post({
        title: title,
        content: content,
        imageUrl: imageUrl,
        creator: {
            name: 'Louis'
        }
    });
    post.save()
        .then(result => {
            res.status(201).json({
                message: 'Post created successfully!',
                post: result
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }  
            next(err);
        });

};

exports.getPost = (req, res, next) => {
    const postId = req.params.postId;
    Post.findById(new mongoose.Types.ObjectId(postId))
      .then(post => {
        if (!post) {
          const error = new Error('Could not find post.');
          error.statusCode = 404;
          throw error;
        }
        res.status(200).json({ message: 'Post fetched.', post: post });
      })
      .catch(err => {
        console.log('err', err)
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      });

};

