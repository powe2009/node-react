const express = require('express');
const {body} = require('express-validator');

const feedController = require('../controller/feed');

const router = express.Router();

// GET /feed/posts
router.get('/posts', feedController.getPosts);

router.post(
    '/posts',
     [
        body('title')
            .trim()
            .isLength({min: 1}),
        body('content')
            .trim()
            .isLength({min: 6})
    ],
    feedController.createPost
); 

router.put(
    '/posts/:postId',
     [
        body('title')
            .trim()
            .isLength({min: 1}),
        body('content')
            .trim()
            .isLength({min: 6})
    ],
    feedController.updatePost
); 

router.get('/post/:postId', feedController.getPost);
  
module.exports = router;
