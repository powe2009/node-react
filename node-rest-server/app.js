const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const multer = require('multer');

const feedRoutes = require('./routes/feed');    
const localUri = 'mongodb://localhost:27017/node-mongoose';
    
const app = express();

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});

const fileFilter = (req, file, cb) => {
    file.mimetype === 'image/png' || 'image/jpg' || 'image/jpeg' ?
        cb(null, true) : cb(null, false);
}

app.use(bodyParser.json());         // application/json v www-form-urlencoded
app.use(
    multer({storage: fileStorage, fileFilter: fileFilter}).single('image')
);
app.use(cors());

app.use( (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, PATCH, FETCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

app.use('/feed', feedRoutes);
app.use('/images', 
    express.static(path.join(__dirname, 'images'))
);

app.use( (error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    res.status(status).json({message: message});
});

mongoose.connect(
        localUri, {useNewUrlParser: true,  useUnifiedTopology: true}
    )
    .then(
        conn => {
            app.listen(7575)
        },
        console.log('server is is listening...')
    )
    .catch(
        error => console.log(error)
    )

// app.listen(7575);   